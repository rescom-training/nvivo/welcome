# Welcome

**Course Description** <br>
If you are working with unstructured data such as interview transcripts, speeches, videos, or even twitter data, then NVivo can help save time during your research.
In this course, you will learn how NVivo can assist you with your research projects. You will understand how NVivo is embedded within your broader research methodology. This course is free to University of Melbourne researchers.
You can participate in this course as often as you like until you have assessed your own learning as successful.  <br> <br>
**Training Format** <br>
This course is separated into individual modules. Whilst the modules are designed to be accessed during an online learning workshop (via Zoom), they are also accessible for those who cannot attend.
Each module consists of four sections with an associated challenge and learning objective attached. Whilst the online training sessions will run for around 40 minutes with 20 minutes for unstructured questions, students are encouraged to work through the sections at their own pace.<br>

**To acccess the course resources, [click here](https://alex-shermon.gitbook.io/rcs-nvivo/)** <br>

**To sign up for training, [click here](rescomunimelb.eventbrite.com.au) <br>**


This forms one part of the Digital Research Skills Support Pack by Research Computing Services, to find out more, [click here](https://research.unimelb.edu.au/infrastructure/research-computing-services/services/training/digital-support-pack).

